---
layout: post
title: "Podcast Per Imparare Italiano!"
date: 2020-04-11 13:32:20 +0300
description: Aprender en forma fácil y divertida, con los audios de **1 Minuto Di Italiano**.
img:  # Add image post (optional)
---
Bienvenidas y bienvenidos, aquí un [audio introductorio]({{ "assets/1_Minuto_Di_Italiano_-_000_-_0._Introduzione.mp3"| absolute_url }}) para saber de qué se trata la propuesta de: 


[**1 MINUTO DI ITALIANO**](https://edumundi.gitlab.io/italiano/)



![icono]({{site.baseurl}}/assets/img/Disegno_senza_titolo.png)

[jekyll-docs]: https://jekyllrb.com/docs/home
[jekyll-gh]:   https://github.com/jekyll/jekyll
[jekyll-talk]: https://talk.jekyllrb.com/
